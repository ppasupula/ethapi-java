package io.bytes.ethApi;

import java.util.List;

public class Main {
    public static void main(String[] args)
    {
        // ropsten
		String provider = "https://ropsten.infura.io/v3/71f27287163440678856b98881dda4ee";
		String toAddress = "0xA32Ea56c289ceEBB35aB5682b2E38a897533d44e";
		String explorerHost = "https://ropsten.etherscan.io";

//        // mainnet
//		String provider = "";
//		String toAddress = "";
//		String explorerHost = "https://etherscan.io";
        
        
    	Eth eth = new Eth(provider);
    	long amountInWei = 10;

        try {
            String hash = eth.makeTx(toAddress, amountInWei);

            System.out.println("\n\n see it here " + explorerHost + "/transaction/" + hash + " \n\n" );

        } catch(Throwable e) {
            System.err.println("\nERROR: Something went wrong: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
